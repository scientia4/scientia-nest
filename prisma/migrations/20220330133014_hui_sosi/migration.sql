/*
  Warnings:

  - A unique constraint covering the columns `[studentTopicId]` on the table `Topic` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `studentTopicId` to the `Topic` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Topic" ADD COLUMN     "studentTopicId" INTEGER NOT NULL;

-- CreateTable
CREATE TABLE "StudentTopic" (
    "id" SERIAL NOT NULL,
    "result" INTEGER NOT NULL,

    CONSTRAINT "StudentTopic_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Question" (
    "id" SERIAL NOT NULL,
    "testId" INTEGER NOT NULL,

    CONSTRAINT "Question_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Topic_studentTopicId_key" ON "Topic"("studentTopicId");

-- AddForeignKey
ALTER TABLE "Topic" ADD CONSTRAINT "Topic_studentTopicId_fkey" FOREIGN KEY ("studentTopicId") REFERENCES "StudentTopic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Question" ADD CONSTRAINT "Question_testId_fkey" FOREIGN KEY ("testId") REFERENCES "Test"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
