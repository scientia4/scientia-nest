-- CreateTable
CREATE TABLE "Topic" (
    "id" SERIAL NOT NULL,
    "plan" TEXT NOT NULL,
    "theory" TEXT NOT NULL,

    CONSTRAINT "Topic_pkey" PRIMARY KEY ("id")
);
