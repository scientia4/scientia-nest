/*
  Warnings:

  - You are about to drop the column `studentTopicId` on the `Topic` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Topic" DROP CONSTRAINT "Topic_studentTopicId_fkey";

-- DropIndex
DROP INDEX "Topic_studentTopicId_key";

-- AlterTable
ALTER TABLE "Topic" DROP COLUMN "studentTopicId";
