/*
  Warnings:

  - You are about to drop the `StudentTopic` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "StudentTopic";

-- CreateTable
CREATE TABLE "SubscriptionTopic" (
    "id" SERIAL NOT NULL,
    "result" INTEGER NOT NULL,
    "userId" INTEGER NOT NULL,
    "topicId" INTEGER NOT NULL,

    CONSTRAINT "SubscriptionTopic_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "SubscriptionTopic_topicId_key" ON "SubscriptionTopic"("topicId");

-- AddForeignKey
ALTER TABLE "SubscriptionTopic" ADD CONSTRAINT "SubscriptionTopic_topicId_fkey" FOREIGN KEY ("topicId") REFERENCES "Topic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SubscriptionTopic" ADD CONSTRAINT "SubscriptionTopic_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
