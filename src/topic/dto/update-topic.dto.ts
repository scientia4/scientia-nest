import { PartialType } from '@nestjs/mapped-types'
import { IsNotEmpty, IsString } from 'class-validator'
import { CreateTopicDto } from './create-topic.dto'

export class UpdateTopicDto extends PartialType(CreateTopicDto) {
  @IsNotEmpty()
  @IsString()
  plan: string

  @IsNotEmpty()
  @IsString()
  theory: string


  @IsNotEmpty()
  @IsString()
  name: string
}
