import { IsNotEmpty, IsNumber, IsString } from "class-validator"

export class CreateTopicDto {
  @IsNotEmpty()
  @IsString()
  name: string

  @IsNotEmpty()
  @IsString()
  plan: string

  @IsNotEmpty()
  @IsString()
  theory: string

  @IsNotEmpty()
  @IsNumber()
  authorId: number
}
