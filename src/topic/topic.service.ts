import { Injectable } from '@nestjs/common'
import { PrismaClientKnownRequestError } from '@prisma/client/runtime'
import { PrismaService } from 'src/prisma.service'
import { PrismaError } from 'src/utils/prismaError'
import { CreateTopicDto } from './dto/create-topic.dto'
import { UpdateTopicDto } from './dto/update-topic.dto'
import { TopicNotFoundException } from './exception/topicNotFound.exception'

@Injectable()
export class TopicService {

  constructor(private readonly prismaService: PrismaService) { }

  // TODO: Не соответвие типов
  async create(createTopicDto) {
    console.log('This action adds a new topic')

    return this.prismaService.topic.create({
      data: createTopicDto,
    })
  }

  async findAll() {
    console.log(`This action returns all topic`)

    return this.prismaService.topic.findMany({
      include: {
        author: true,
        test: true
      }
    })
  }

  async findOne(id: number) {
    console.log(`This action returns a #${id} topic`)

    const post = await this.prismaService.topic.findUnique({
      where: {
        id,
      },
      include: {
        test: {
          include: {
            question: true
          }
        },
        author: true
      },
    })
    if (!post) {
      throw new TopicNotFoundException(id)
    }
    return post
  }

  // TODO: Не соответвие типов

  async update(id: number, updateTopicDto) {
    console.log(`This action updates a #${id} topic`)

    try {
      return await this.prismaService.topic.update({
        data: {
          ...updateTopicDto,
          id: undefined,
        },
        where: {
          id,
        },
      })
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new TopicNotFoundException(id)
      }
      throw error
    }
  }

  async remove(id: number) {
    console.log(`This action removes a #${id} topic`)

    try {
      return this.prismaService.topic.delete({
        where: {
          id,
        },
      })
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new TopicNotFoundException(id)
      }
      throw error
    }
  }
}
