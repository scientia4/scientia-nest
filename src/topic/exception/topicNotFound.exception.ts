import { NotFoundException } from '@nestjs/common';

export class TopicNotFoundException extends NotFoundException {
    constructor(postId: number) {
        super(`Topic with id ${postId} not found`);
    }
}