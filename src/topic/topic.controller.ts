import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { TopicService } from './topic.service'
import { CreateTopicDto } from './dto/create-topic.dto'
import { UpdateTopicDto } from './dto/update-topic.dto'
import { FindOneParams } from 'src/utils/findOneParams'

@Controller('topic')
export class TopicController {
  constructor(private readonly topicService: TopicService) { }

  @Post()
  async create(@Body() createTopicDto: CreateTopicDto) {



    const { plan, theory, authorId, name } = createTopicDto

    return await this.topicService.create({
      plan, theory, name,
      author: {
        connect: { id: authorId },
      },
    })
  }

  @Get()
  async findAll() {
    return await this.topicService.findAll()
  }

  @Get(':id')
  async findOne(@Param() { id }: FindOneParams) {
    return await this.topicService.findOne(+id)
  }

  @Patch(':id')
  async update(@Param() { id }: FindOneParams, @Body() updateTopicDto: UpdateTopicDto) {
    return await this.topicService.update(+id, updateTopicDto)
  }

  @Delete(':id')
  async remove(@Param() { id }: FindOneParams) {
    return await this.topicService.remove(+id)
  }
}
