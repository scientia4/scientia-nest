import { Injectable } from '@nestjs/common'
import { PrismaClientKnownRequestError } from '@prisma/client/runtime'
import { PrismaService } from 'src/prisma.service'
import { PrismaError } from 'src/utils/prismaError'
import { CreateSubscriptionTopicDto } from './dto/create-subscription-topic.dto'
import { UpdateSubscriptionTopicDto } from './dto/update-subscription-topic.dto'
import { SubscriptionTopicNotFoundException } from './exception/subscriptionTopicNotFound.exception'

@Injectable()
export class SubscriptionTopicService {

  constructor(private readonly prismaService: PrismaService) { }

  // TODO: Не соответвие типов
  async create(createSubscriptionTopicDto) {
    console.log('This action adds a new subscriptionTopic')

    return this.prismaService.subscriptionTopic.create({
      data: createSubscriptionTopicDto,
    })
  }

  async findAll() {
    console.log(`This action returns all subscriptionTopic`)

    return this.prismaService.subscriptionTopic.findMany()
  }

  async findOne(id: number) {
    console.log(`This action returns a #${id} subscriptionTopic`)

    const post = await this.prismaService.subscriptionTopic.findUnique({
      where: {
        id,
      },
    })
    if (!post) {
      throw new SubscriptionTopicNotFoundException(id)
    }
    return post
  }

  // TODO: Не соответвие типов

  async update(id: number, updateSubscriptionTopicDto) {
    console.log(`This action updates a #${id} subscriptionTopic`)

    try {
      return await this.prismaService.subscriptionTopic.update({
        data: {
          ...updateSubscriptionTopicDto,
          id: undefined,
        },
        where: {
          id,
        },
      })
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new SubscriptionTopicNotFoundException(id)
      }
      throw error
    }
  }

  async remove(id: number) {
    console.log(`This action removes a #${id} subscriptionTopic`)

    try {
      return this.prismaService.subscriptionTopic.delete({
        where: {
          id,
        },
      })
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new SubscriptionTopicNotFoundException(id)
      }
      throw error
    }
  }
}
