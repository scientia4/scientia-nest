import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { SubscriptionTopicService } from './subscription-topic.service'
import { CreateSubscriptionTopicDto } from './dto/create-subscription-topic.dto'
import { UpdateSubscriptionTopicDto } from './dto/update-subscription-topic.dto'
import { FindOneParams } from 'src/utils/findOneParams'

@Controller('subscriptionTopic')
export class SubscriptionTopicController {
  constructor(private readonly subscriptionTopicService: SubscriptionTopicService) { }

  @Post()
  async create(@Body() createSubscriptionTopicDto: CreateSubscriptionTopicDto) {

    const { userId, topicId } = createSubscriptionTopicDto

    return await this.subscriptionTopicService.create({
      user: {
        connect: { id: userId },
      },
      topic: {
        connect: { id: topicId },
      },
    })
  }

  @Get()
  async findAll() {
    return await this.subscriptionTopicService.findAll()
  }

  @Get(':id')
  async findOne(@Param() { id }: FindOneParams) {
    return await this.subscriptionTopicService.findOne(+id)
  }

  @Patch(':id')
  async update(@Param() { id }: FindOneParams, @Body() updateSubscriptionTopicDto: UpdateSubscriptionTopicDto) {
    return await this.subscriptionTopicService.update(+id, updateSubscriptionTopicDto)
  }

  @Delete(':id')
  async remove(@Param() { id }: FindOneParams) {
    return await this.subscriptionTopicService.remove(+id)
  }
}
