import { IsNotEmpty, IsNumber } from 'class-validator'

export class CreateSubscriptionTopicDto {



  @IsNotEmpty()
  @IsNumber()
  userId: number


  @IsNotEmpty()
  @IsNumber()
  topicId: number

}
