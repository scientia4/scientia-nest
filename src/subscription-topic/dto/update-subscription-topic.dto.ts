import { PartialType } from '@nestjs/mapped-types';
import { CreateSubscriptionTopicDto } from './create-subscription-topic.dto';

export class UpdateSubscriptionTopicDto extends PartialType(CreateSubscriptionTopicDto) {}
