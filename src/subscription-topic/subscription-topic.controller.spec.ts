import { Test, TestingModule } from '@nestjs/testing';
import { SubscriptionTopicController } from './subscription-topic.controller';
import { SubscriptionTopicService } from './subscription-topic.service';

describe('SubscriptionTopicController', () => {
  let controller: SubscriptionTopicController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubscriptionTopicController],
      providers: [SubscriptionTopicService],
    }).compile();

    controller = module.get<SubscriptionTopicController>(SubscriptionTopicController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
