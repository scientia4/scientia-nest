import { Test, TestingModule } from '@nestjs/testing';
import { SubscriptionTopicService } from './subscription-topic.service';

describe('SubscriptionTopicService', () => {
  let service: SubscriptionTopicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SubscriptionTopicService],
    }).compile();

    service = module.get<SubscriptionTopicService>(SubscriptionTopicService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
