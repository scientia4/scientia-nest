import { NotFoundException } from '@nestjs/common'

export class SubscriptionTopicNotFoundException extends NotFoundException {
  constructor(subscriptionTopicId: number) {
    super(`SubscriptionTopic with id ${subscriptionTopicId} not found`)
  }
}