import { Module } from '@nestjs/common'
import { SubscriptionTopicService } from './subscription-topic.service'
import { SubscriptionTopicController } from './subscription-topic.controller'
import { PrismaModule } from 'src/prisma.module'

@Module({
  imports: [PrismaModule],
  controllers: [SubscriptionTopicController],
  providers: [SubscriptionTopicService]
})
export class SubscriptionTopicModule { }
