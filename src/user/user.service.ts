import { Injectable } from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { PrismaService } from 'src/prisma.service';
import { PrismaError } from 'src/utils/prismaError';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserNotFoundException } from './exception/userNotFound.exception';

@Injectable()
export class UserService {

  constructor(private readonly prismaService: PrismaService) { }

  // TODO: Не соответвие типов
  async create(createUserDto) {
    console.log('This action adds a new user');

    return this.prismaService.user.create({
      data: createUserDto,
    });
  }

  async findAll() {
    console.log(`This action returns all user`);

    return this.prismaService.user.findMany();
  }

  async findOne(id: number) {
    console.log(`This action returns a #${id} user`);

    const post = await this.prismaService.user.findUnique({
      where: {
        id,
      },
      include: {
        subscriptionTopic: {
          include: {
            topic: true, // Include post categories
          },
        },

      },
    });
    if (!post) {
      throw new UserNotFoundException(id);
    }
    return post;
  }

  // TODO: Не соответвие типов

  async update(id: number, updateUserDto) {
    console.log(`This action updates a #${id} user`);

    try {
      return await this.prismaService.user.update({
        data: {
          ...updateUserDto,
          id: undefined,
        },
        where: {
          id,
        },
      });
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new UserNotFoundException(id);
      }
      throw error;
    }
  }

  async remove(id: number) {
    console.log(`This action removes a #${id} user`);

    try {
      return this.prismaService.user.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new UserNotFoundException(id);
      }
      throw error;
    }
  }
}
