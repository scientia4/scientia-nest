import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { UserService } from './user.service'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { FindOneParams } from 'src/utils/findOneParams'

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    return await this.userService.create(createUserDto)
  }

  @Get()
  async findAll() {
    return await this.userService.findAll()
  }

  @Get(':id')
  async findOne(@Param() { id }: FindOneParams) {
    return await this.userService.findOne(+id)
  }

  @Patch(':id')
  async update(@Param() { id }: FindOneParams, @Body() updateUserDto: UpdateUserDto) {
    return await this.userService.update(+id, updateUserDto)
  }

  @Delete(':id')
  async remove(@Param() { id }: FindOneParams) {
    return await this.userService.remove(+id)
  }
}
