import { Injectable } from '@nestjs/common'
import { PrismaClientKnownRequestError } from '@prisma/client/runtime'
import { PrismaService } from 'src/prisma.service'
import { PrismaError } from 'src/utils/prismaError'
import { CreateTestDto } from './dto/create-test.dto'
import { UpdateTestDto } from './dto/update-test.dto'
import { TestNotFoundException } from './exception/testNotFound.exception'

@Injectable()
export class TestService {

  constructor(private readonly prismaService: PrismaService) { }

  // TODO: Не соответвие типов
  async create(createTestDto) {
    console.log('This action adds a new test')

    return this.prismaService.test.create({
      data: createTestDto,
    })
  }

  async findAll() {
    console.log(`This action returns all test`)

    return this.prismaService.test.findMany()
  }

  async findOne(id: number) {
    console.log(`This action returns a #${id} test`)

    const post = await this.prismaService.test.findUnique({
      where: {
        id,
      },
    })
    if (!post) {
      throw new TestNotFoundException(id)
    }
    return post
  }

  // TODO: Не соответвие типов

  async update(id: number, updateTestDto) {
    console.log(`This action updates a #${id} test`)

    try {
      return await this.prismaService.test.update({
        data: {
          ...updateTestDto,
          id: undefined,
        },
        where: {
          id,
        },
      })
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new TestNotFoundException(id)
      }
      throw error
    }
  }

  async remove(id: number) {
    console.log(`This action removes a #${id} test`)

    try {
      return this.prismaService.test.delete({
        where: {
          id,
        },
      })
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new TestNotFoundException(id)
      }
      throw error
    }
  }
}
