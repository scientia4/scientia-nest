import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { TestService } from './test.service'
import { CreateTestDto } from './dto/create-test.dto'
import { UpdateTestDto } from './dto/update-test.dto'

@Controller('test')
export class TestController {
  constructor(private readonly testService: TestService) { }

  @Post()
  async create(@Body() createTestDto: CreateTestDto) {


    const { topicId } = createTestDto
    return await this.testService.create({
      topic: {
        connect: { id: topicId },
      },
    })
  }

  @Get()
  async findAll() {
    return await this.testService.findAll()
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.testService.findOne(+id)
  }

  
  // TODO: Если нет топика к которому необходимо связать test то вылетает 500
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateTestDto: UpdateTestDto) {
    return await this.testService.update(+id, updateTestDto)
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.testService.remove(+id)
  }
}
