import { NotFoundException } from '@nestjs/common'

export class TestNotFoundException extends NotFoundException {
  constructor(testId: number) {
    super(`Test with id ${testId} not found`)
  }
}