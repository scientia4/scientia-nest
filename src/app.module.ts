import { PrismaModule } from './prisma.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TopicModule } from './topic/topic.module';
import { TestModule } from './test/test.module';
import { UserModule } from './user/user.module';
import { QuestionModule } from './question/question.module';
import { SubscriptionTopicModule } from './subscription-topic/subscription-topic.module';

@Module({
  imports: [
        PrismaModule,
        TopicModule,
        TestModule,
        UserModule,
        QuestionModule,
        SubscriptionTopicModule, ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
