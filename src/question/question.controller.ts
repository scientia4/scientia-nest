import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { QuestionService } from './question.service'
import { CreateQuestionDto } from './dto/create-question.dto'
import { UpdateQuestionDto } from './dto/update-question.dto'
import { FindOneParams } from 'src/utils/findOneParams'

@Controller('question')
export class QuestionController {
  constructor(private readonly questionService: QuestionService) { }

  @Post()
  async create(@Body() createQuestionDto: CreateQuestionDto) {

    const { testId } = createQuestionDto

    return await this.questionService.create({
      test: {
        connect: { id: testId },
      },
    })
  }

  @Get()
  async findAll() {
    return await this.questionService.findAll()
  }

  @Get(':id')
  async findOne(@Param() { id }: FindOneParams) {
    return await this.questionService.findOne(+id)
  }

  @Patch(':id')
  async update(@Param() { id }: FindOneParams, @Body() updateQuestionDto: UpdateQuestionDto) {
    return await this.questionService.update(+id, updateQuestionDto)
  }

  @Delete(':id')
  async remove(@Param() { id }: FindOneParams) {
    return await this.questionService.remove(+id)
  }
}
