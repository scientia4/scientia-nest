import { Injectable } from '@nestjs/common'
import { PrismaClientKnownRequestError } from '@prisma/client/runtime'
import { PrismaService } from 'src/prisma.service'
import { PrismaError } from 'src/utils/prismaError'
import { CreateQuestionDto } from './dto/create-question.dto'
import { UpdateQuestionDto } from './dto/update-question.dto'
import { QuestionNotFoundException } from './exception/questionNotFound.exception'

@Injectable()
export class QuestionService {

  constructor(private readonly prismaService: PrismaService) { }

  // TODO: Не соответвие типов
  async create(createQuestionDto) {
    console.log('This action adds a new question')

    return this.prismaService.question.create({
      data: createQuestionDto,
    })
  }

  async findAll() {
    console.log(`This action returns all question`)

    return this.prismaService.question.findMany()
  }

  async findOne(id: number) {
    console.log(`This action returns a #${id} question`)

    const post = await this.prismaService.question.findUnique({
      where: {
        id,
      },
    })
    if (!post) {
      throw new QuestionNotFoundException(id)
    }
    return post
  }

  // TODO: Не соответвие типов

  async update(id: number, updateQuestionDto) {
    console.log(`This action updates a #${id} question`)

    try {
      return await this.prismaService.question.update({
        data: {
          ...updateQuestionDto,
          id: undefined,
        },
        where: {
          id,
        },
      })
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new QuestionNotFoundException(id)
      }
      throw error
    }
  }

  async remove(id: number) {
    console.log(`This action removes a #${id} question`)

    try {
      return this.prismaService.question.delete({
        where: {
          id,
        },
      })
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError &&
        error.code === PrismaError.RecordDoesNotExist
      ) {
        throw new QuestionNotFoundException(id)
      }
      throw error
    }
  }
}
