import { IsNotEmpty, IsNumber } from 'class-validator'

export class CreateQuestionDto {

  @IsNotEmpty()
  @IsNumber()
  testId: number
}
