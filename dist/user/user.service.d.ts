import { PrismaService } from 'src/prisma.service';
export declare class UserService {
    private readonly prismaService;
    constructor(prismaService: PrismaService);
    create(createUserDto: any): Promise<import(".prisma/client").User>;
    findAll(): Promise<import(".prisma/client").User[]>;
    findOne(id: number): Promise<import(".prisma/client").User & {
        subscriptionTopic: (import(".prisma/client").SubscriptionTopic & {
            topic: import(".prisma/client").Topic;
        })[];
    }>;
    update(id: number, updateUserDto: any): Promise<import(".prisma/client").User>;
    remove(id: number): Promise<import(".prisma/client").User>;
}
