import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { FindOneParams } from 'src/utils/findOneParams';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    create(createUserDto: CreateUserDto): Promise<import(".prisma/client").User>;
    findAll(): Promise<import(".prisma/client").User[]>;
    findOne({ id }: FindOneParams): Promise<import(".prisma/client").User & {
        subscriptionTopic: (import(".prisma/client").SubscriptionTopic & {
            topic: import(".prisma/client").Topic;
        })[];
    }>;
    update({ id }: FindOneParams, updateUserDto: UpdateUserDto): Promise<import(".prisma/client").User>;
    remove({ id }: FindOneParams): Promise<import(".prisma/client").User>;
}
