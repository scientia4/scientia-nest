import { QuestionService } from './question.service';
import { CreateQuestionDto } from './dto/create-question.dto';
import { UpdateQuestionDto } from './dto/update-question.dto';
import { FindOneParams } from 'src/utils/findOneParams';
export declare class QuestionController {
    private readonly questionService;
    constructor(questionService: QuestionService);
    create(createQuestionDto: CreateQuestionDto): Promise<import(".prisma/client").Question>;
    findAll(): Promise<import(".prisma/client").Question[]>;
    findOne({ id }: FindOneParams): Promise<import(".prisma/client").Question>;
    update({ id }: FindOneParams, updateQuestionDto: UpdateQuestionDto): Promise<import(".prisma/client").Question>;
    remove({ id }: FindOneParams): Promise<import(".prisma/client").Question>;
}
