import { NotFoundException } from '@nestjs/common';
export declare class QuestionNotFoundException extends NotFoundException {
    constructor(questionId: number);
}
