"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionNotFoundException = void 0;
const common_1 = require("@nestjs/common");
class QuestionNotFoundException extends common_1.NotFoundException {
    constructor(questionId) {
        super(`Question with id ${questionId} not found`);
    }
}
exports.QuestionNotFoundException = QuestionNotFoundException;
//# sourceMappingURL=questionNotFound.exception.js.map