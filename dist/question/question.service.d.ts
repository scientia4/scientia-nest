import { PrismaService } from 'src/prisma.service';
export declare class QuestionService {
    private readonly prismaService;
    constructor(prismaService: PrismaService);
    create(createQuestionDto: any): Promise<import(".prisma/client").Question>;
    findAll(): Promise<import(".prisma/client").Question[]>;
    findOne(id: number): Promise<import(".prisma/client").Question>;
    update(id: number, updateQuestionDto: any): Promise<import(".prisma/client").Question>;
    remove(id: number): Promise<import(".prisma/client").Question>;
}
