"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionService = void 0;
const common_1 = require("@nestjs/common");
const runtime_1 = require("@prisma/client/runtime");
const prisma_service_1 = require("../prisma.service");
const prismaError_1 = require("../utils/prismaError");
const questionNotFound_exception_1 = require("./exception/questionNotFound.exception");
let QuestionService = class QuestionService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async create(createQuestionDto) {
        console.log('This action adds a new question');
        return this.prismaService.question.create({
            data: createQuestionDto,
        });
    }
    async findAll() {
        console.log(`This action returns all question`);
        return this.prismaService.question.findMany();
    }
    async findOne(id) {
        console.log(`This action returns a #${id} question`);
        const post = await this.prismaService.question.findUnique({
            where: {
                id,
            },
        });
        if (!post) {
            throw new questionNotFound_exception_1.QuestionNotFoundException(id);
        }
        return post;
    }
    async update(id, updateQuestionDto) {
        console.log(`This action updates a #${id} question`);
        try {
            return await this.prismaService.question.update({
                data: Object.assign(Object.assign({}, updateQuestionDto), { id: undefined }),
                where: {
                    id,
                },
            });
        }
        catch (error) {
            if (error instanceof runtime_1.PrismaClientKnownRequestError &&
                error.code === prismaError_1.PrismaError.RecordDoesNotExist) {
                throw new questionNotFound_exception_1.QuestionNotFoundException(id);
            }
            throw error;
        }
    }
    async remove(id) {
        console.log(`This action removes a #${id} question`);
        try {
            return this.prismaService.question.delete({
                where: {
                    id,
                },
            });
        }
        catch (error) {
            if (error instanceof runtime_1.PrismaClientKnownRequestError &&
                error.code === prismaError_1.PrismaError.RecordDoesNotExist) {
                throw new questionNotFound_exception_1.QuestionNotFoundException(id);
            }
            throw error;
        }
    }
};
QuestionService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], QuestionService);
exports.QuestionService = QuestionService;
//# sourceMappingURL=question.service.js.map