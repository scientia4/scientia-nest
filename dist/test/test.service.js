"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestService = void 0;
const common_1 = require("@nestjs/common");
const runtime_1 = require("@prisma/client/runtime");
const prisma_service_1 = require("../prisma.service");
const prismaError_1 = require("../utils/prismaError");
const testNotFound_exception_1 = require("./exception/testNotFound.exception");
let TestService = class TestService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async create(createTestDto) {
        console.log('This action adds a new test');
        return this.prismaService.test.create({
            data: createTestDto,
        });
    }
    async findAll() {
        console.log(`This action returns all test`);
        return this.prismaService.test.findMany();
    }
    async findOne(id) {
        console.log(`This action returns a #${id} test`);
        const post = await this.prismaService.test.findUnique({
            where: {
                id,
            },
        });
        if (!post) {
            throw new testNotFound_exception_1.TestNotFoundException(id);
        }
        return post;
    }
    async update(id, updateTestDto) {
        console.log(`This action updates a #${id} test`);
        try {
            return await this.prismaService.test.update({
                data: Object.assign(Object.assign({}, updateTestDto), { id: undefined }),
                where: {
                    id,
                },
            });
        }
        catch (error) {
            if (error instanceof runtime_1.PrismaClientKnownRequestError &&
                error.code === prismaError_1.PrismaError.RecordDoesNotExist) {
                throw new testNotFound_exception_1.TestNotFoundException(id);
            }
            throw error;
        }
    }
    async remove(id) {
        console.log(`This action removes a #${id} test`);
        try {
            return this.prismaService.test.delete({
                where: {
                    id,
                },
            });
        }
        catch (error) {
            if (error instanceof runtime_1.PrismaClientKnownRequestError &&
                error.code === prismaError_1.PrismaError.RecordDoesNotExist) {
                throw new testNotFound_exception_1.TestNotFoundException(id);
            }
            throw error;
        }
    }
};
TestService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], TestService);
exports.TestService = TestService;
//# sourceMappingURL=test.service.js.map