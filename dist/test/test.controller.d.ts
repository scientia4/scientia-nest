import { TestService } from './test.service';
import { CreateTestDto } from './dto/create-test.dto';
import { UpdateTestDto } from './dto/update-test.dto';
export declare class TestController {
    private readonly testService;
    constructor(testService: TestService);
    create(createTestDto: CreateTestDto): Promise<import(".prisma/client").Test>;
    findAll(): Promise<import(".prisma/client").Test[]>;
    findOne(id: string): Promise<import(".prisma/client").Test>;
    update(id: string, updateTestDto: UpdateTestDto): Promise<import(".prisma/client").Test>;
    remove(id: string): Promise<import(".prisma/client").Test>;
}
