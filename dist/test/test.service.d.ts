import { PrismaService } from 'src/prisma.service';
export declare class TestService {
    private readonly prismaService;
    constructor(prismaService: PrismaService);
    create(createTestDto: any): Promise<import(".prisma/client").Test>;
    findAll(): Promise<import(".prisma/client").Test[]>;
    findOne(id: number): Promise<import(".prisma/client").Test>;
    update(id: number, updateTestDto: any): Promise<import(".prisma/client").Test>;
    remove(id: number): Promise<import(".prisma/client").Test>;
}
