"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestNotFoundException = void 0;
const common_1 = require("@nestjs/common");
class TestNotFoundException extends common_1.NotFoundException {
    constructor(testId) {
        super(`Test with id ${testId} not found`);
    }
}
exports.TestNotFoundException = TestNotFoundException;
//# sourceMappingURL=testNotFound.exception.js.map