import { NotFoundException } from '@nestjs/common';
export declare class TestNotFoundException extends NotFoundException {
    constructor(testId: number);
}
