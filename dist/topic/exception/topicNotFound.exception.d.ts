import { NotFoundException } from '@nestjs/common';
export declare class TopicNotFoundException extends NotFoundException {
    constructor(postId: number);
}
