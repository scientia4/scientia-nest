"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TopicNotFoundException = void 0;
const common_1 = require("@nestjs/common");
class TopicNotFoundException extends common_1.NotFoundException {
    constructor(postId) {
        super(`Topic with id ${postId} not found`);
    }
}
exports.TopicNotFoundException = TopicNotFoundException;
//# sourceMappingURL=topicNotFound.exception.js.map