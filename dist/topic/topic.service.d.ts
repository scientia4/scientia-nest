import { PrismaService } from 'src/prisma.service';
export declare class TopicService {
    private readonly prismaService;
    constructor(prismaService: PrismaService);
    create(createTopicDto: any): Promise<import(".prisma/client").Topic>;
    findAll(): Promise<(import(".prisma/client").Topic & {
        author: import(".prisma/client").User;
        test: import(".prisma/client").Test[];
    })[]>;
    findOne(id: number): Promise<import(".prisma/client").Topic & {
        author: import(".prisma/client").User;
        test: (import(".prisma/client").Test & {
            question: import(".prisma/client").Question[];
        })[];
    }>;
    update(id: number, updateTopicDto: any): Promise<import(".prisma/client").Topic>;
    remove(id: number): Promise<import(".prisma/client").Topic>;
}
