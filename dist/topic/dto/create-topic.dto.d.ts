export declare class CreateTopicDto {
    name: string;
    plan: string;
    theory: string;
    authorId: number;
}
