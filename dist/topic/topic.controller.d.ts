import { TopicService } from './topic.service';
import { CreateTopicDto } from './dto/create-topic.dto';
import { UpdateTopicDto } from './dto/update-topic.dto';
import { FindOneParams } from 'src/utils/findOneParams';
export declare class TopicController {
    private readonly topicService;
    constructor(topicService: TopicService);
    create(createTopicDto: CreateTopicDto): Promise<import(".prisma/client").Topic>;
    findAll(): Promise<(import(".prisma/client").Topic & {
        author: import(".prisma/client").User;
        test: import(".prisma/client").Test[];
    })[]>;
    findOne({ id }: FindOneParams): Promise<import(".prisma/client").Topic & {
        author: import(".prisma/client").User;
        test: (import(".prisma/client").Test & {
            question: import(".prisma/client").Question[];
        })[];
    }>;
    update({ id }: FindOneParams, updateTopicDto: UpdateTopicDto): Promise<import(".prisma/client").Topic>;
    remove({ id }: FindOneParams): Promise<import(".prisma/client").Topic>;
}
