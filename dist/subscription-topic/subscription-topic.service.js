"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionTopicService = void 0;
const common_1 = require("@nestjs/common");
const runtime_1 = require("@prisma/client/runtime");
const prisma_service_1 = require("../prisma.service");
const prismaError_1 = require("../utils/prismaError");
const subscriptionTopicNotFound_exception_1 = require("./exception/subscriptionTopicNotFound.exception");
let SubscriptionTopicService = class SubscriptionTopicService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async create(createSubscriptionTopicDto) {
        console.log('This action adds a new subscriptionTopic');
        return this.prismaService.subscriptionTopic.create({
            data: createSubscriptionTopicDto,
        });
    }
    async findAll() {
        console.log(`This action returns all subscriptionTopic`);
        return this.prismaService.subscriptionTopic.findMany();
    }
    async findOne(id) {
        console.log(`This action returns a #${id} subscriptionTopic`);
        const post = await this.prismaService.subscriptionTopic.findUnique({
            where: {
                id,
            },
        });
        if (!post) {
            throw new subscriptionTopicNotFound_exception_1.SubscriptionTopicNotFoundException(id);
        }
        return post;
    }
    async update(id, updateSubscriptionTopicDto) {
        console.log(`This action updates a #${id} subscriptionTopic`);
        try {
            return await this.prismaService.subscriptionTopic.update({
                data: Object.assign(Object.assign({}, updateSubscriptionTopicDto), { id: undefined }),
                where: {
                    id,
                },
            });
        }
        catch (error) {
            if (error instanceof runtime_1.PrismaClientKnownRequestError &&
                error.code === prismaError_1.PrismaError.RecordDoesNotExist) {
                throw new subscriptionTopicNotFound_exception_1.SubscriptionTopicNotFoundException(id);
            }
            throw error;
        }
    }
    async remove(id) {
        console.log(`This action removes a #${id} subscriptionTopic`);
        try {
            return this.prismaService.subscriptionTopic.delete({
                where: {
                    id,
                },
            });
        }
        catch (error) {
            if (error instanceof runtime_1.PrismaClientKnownRequestError &&
                error.code === prismaError_1.PrismaError.RecordDoesNotExist) {
                throw new subscriptionTopicNotFound_exception_1.SubscriptionTopicNotFoundException(id);
            }
            throw error;
        }
    }
};
SubscriptionTopicService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], SubscriptionTopicService);
exports.SubscriptionTopicService = SubscriptionTopicService;
//# sourceMappingURL=subscription-topic.service.js.map