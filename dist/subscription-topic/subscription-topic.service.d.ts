import { PrismaService } from 'src/prisma.service';
export declare class SubscriptionTopicService {
    private readonly prismaService;
    constructor(prismaService: PrismaService);
    create(createSubscriptionTopicDto: any): Promise<import(".prisma/client").SubscriptionTopic>;
    findAll(): Promise<import(".prisma/client").SubscriptionTopic[]>;
    findOne(id: number): Promise<import(".prisma/client").SubscriptionTopic>;
    update(id: number, updateSubscriptionTopicDto: any): Promise<import(".prisma/client").SubscriptionTopic>;
    remove(id: number): Promise<import(".prisma/client").SubscriptionTopic>;
}
