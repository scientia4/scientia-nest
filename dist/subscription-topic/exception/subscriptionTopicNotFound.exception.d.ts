import { NotFoundException } from '@nestjs/common';
export declare class SubscriptionTopicNotFoundException extends NotFoundException {
    constructor(subscriptionTopicId: number);
}
