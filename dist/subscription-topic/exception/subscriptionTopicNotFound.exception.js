"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionTopicNotFoundException = void 0;
const common_1 = require("@nestjs/common");
class SubscriptionTopicNotFoundException extends common_1.NotFoundException {
    constructor(subscriptionTopicId) {
        super(`SubscriptionTopic with id ${subscriptionTopicId} not found`);
    }
}
exports.SubscriptionTopicNotFoundException = SubscriptionTopicNotFoundException;
//# sourceMappingURL=subscriptionTopicNotFound.exception.js.map