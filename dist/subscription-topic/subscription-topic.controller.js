"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionTopicController = void 0;
const common_1 = require("@nestjs/common");
const subscription_topic_service_1 = require("./subscription-topic.service");
const create_subscription_topic_dto_1 = require("./dto/create-subscription-topic.dto");
const update_subscription_topic_dto_1 = require("./dto/update-subscription-topic.dto");
const findOneParams_1 = require("../utils/findOneParams");
let SubscriptionTopicController = class SubscriptionTopicController {
    constructor(subscriptionTopicService) {
        this.subscriptionTopicService = subscriptionTopicService;
    }
    async create(createSubscriptionTopicDto) {
        const { userId, topicId } = createSubscriptionTopicDto;
        return await this.subscriptionTopicService.create({
            user: {
                connect: { id: userId },
            },
            topic: {
                connect: { id: topicId },
            },
        });
    }
    async findAll() {
        return await this.subscriptionTopicService.findAll();
    }
    async findOne({ id }) {
        return await this.subscriptionTopicService.findOne(+id);
    }
    async update({ id }, updateSubscriptionTopicDto) {
        return await this.subscriptionTopicService.update(+id, updateSubscriptionTopicDto);
    }
    async remove({ id }) {
        return await this.subscriptionTopicService.remove(+id);
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_subscription_topic_dto_1.CreateSubscriptionTopicDto]),
    __metadata("design:returntype", Promise)
], SubscriptionTopicController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], SubscriptionTopicController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [findOneParams_1.FindOneParams]),
    __metadata("design:returntype", Promise)
], SubscriptionTopicController.prototype, "findOne", null);
__decorate([
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [findOneParams_1.FindOneParams, update_subscription_topic_dto_1.UpdateSubscriptionTopicDto]),
    __metadata("design:returntype", Promise)
], SubscriptionTopicController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [findOneParams_1.FindOneParams]),
    __metadata("design:returntype", Promise)
], SubscriptionTopicController.prototype, "remove", null);
SubscriptionTopicController = __decorate([
    (0, common_1.Controller)('subscriptionTopic'),
    __metadata("design:paramtypes", [subscription_topic_service_1.SubscriptionTopicService])
], SubscriptionTopicController);
exports.SubscriptionTopicController = SubscriptionTopicController;
//# sourceMappingURL=subscription-topic.controller.js.map