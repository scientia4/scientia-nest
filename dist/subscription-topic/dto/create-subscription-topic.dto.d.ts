export declare class CreateSubscriptionTopicDto {
    userId: number;
    topicId: number;
}
