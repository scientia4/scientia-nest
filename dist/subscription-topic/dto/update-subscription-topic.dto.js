"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateSubscriptionTopicDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const create_subscription_topic_dto_1 = require("./create-subscription-topic.dto");
class UpdateSubscriptionTopicDto extends (0, mapped_types_1.PartialType)(create_subscription_topic_dto_1.CreateSubscriptionTopicDto) {
}
exports.UpdateSubscriptionTopicDto = UpdateSubscriptionTopicDto;
//# sourceMappingURL=update-subscription-topic.dto.js.map