import { CreateSubscriptionTopicDto } from './create-subscription-topic.dto';
declare const UpdateSubscriptionTopicDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateSubscriptionTopicDto>>;
export declare class UpdateSubscriptionTopicDto extends UpdateSubscriptionTopicDto_base {
}
export {};
