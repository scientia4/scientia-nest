import { SubscriptionTopicService } from './subscription-topic.service';
import { CreateSubscriptionTopicDto } from './dto/create-subscription-topic.dto';
import { UpdateSubscriptionTopicDto } from './dto/update-subscription-topic.dto';
import { FindOneParams } from 'src/utils/findOneParams';
export declare class SubscriptionTopicController {
    private readonly subscriptionTopicService;
    constructor(subscriptionTopicService: SubscriptionTopicService);
    create(createSubscriptionTopicDto: CreateSubscriptionTopicDto): Promise<import(".prisma/client").SubscriptionTopic>;
    findAll(): Promise<import(".prisma/client").SubscriptionTopic[]>;
    findOne({ id }: FindOneParams): Promise<import(".prisma/client").SubscriptionTopic>;
    update({ id }: FindOneParams, updateSubscriptionTopicDto: UpdateSubscriptionTopicDto): Promise<import(".prisma/client").SubscriptionTopic>;
    remove({ id }: FindOneParams): Promise<import(".prisma/client").SubscriptionTopic>;
}
